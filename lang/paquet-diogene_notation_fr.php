<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_notation.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_notation_description' => 'Complément notation pour "Diogene"',
	'diogene_notation_nom' => 'Diogene - Notations',
	'diogene_notation_slogan' => 'Complément notation pour "Diogene"'
);
