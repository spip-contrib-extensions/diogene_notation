<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_notation?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_notation_description' => 'Ratings add-on for "Diogene".',
	'diogene_notation_nom' => 'Diogene - Ratings',
	'diogene_notation_slogan' => 'Ratings add-on for "Diogene"'
);
